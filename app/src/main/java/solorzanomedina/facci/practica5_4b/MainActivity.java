package solorzanomedina.facci.practica5_4b;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener
{
    Button FragUno, FragDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragDos = findViewById(R.id.btnFragDos);
        FragUno = findViewById(R.id.btnFragUno);
        FragUno.setOnClickListener(this);
        FragDos.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFragUno:
                FragUno fragUno = new FragUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragUno);
                transactionUno.commit();
                break;
            case R.id.btnFragDos:
                FragDos fragDos = new FragDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragDos);
                transactionDos.commit();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button botonAutentificar = dialogoLogin.findViewById(R.id.btnAutentificar);
                final EditText etusuario = dialogoLogin.findViewById(R.id.etUsuario);
                final EditText etclave = dialogoLogin.findViewById(R.id.etClave);

                botonAutentificar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, etusuario.getText().toString()+ " \n"+ etclave.getText().toString(),Toast.LENGTH_LONG).show();

                    }
                });
                dialogoLogin.show();

                break;
            case R.id.opcionRegistrar:
                Dialog dialogoRegistrar = new Dialog(MainActivity.this);
                dialogoRegistrar.setContentView(R.layout.dlg_registrar);
                Button btnRegistrar = dialogoRegistrar.findViewById(R.id.btnRegistrar);
                final EditText etnombres = dialogoRegistrar.findViewById(R.id.etNombre);
                final EditText etapellidos = dialogoRegistrar.findViewById(R.id.etApellidos);
                final EditText etemail = dialogoRegistrar.findViewById(R.id.etEmail);
                final EditText etUsuario = dialogoRegistrar.findViewById(R.id.etUsuario);

                btnRegistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this,
                                etnombres.getText().toString()+ " \n" + etapellidos.getText().toString() +
                                        "\n"+etemail.getText().toString()+"\n"+etUsuario.getText().toString(),
                                Toast.LENGTH_LONG).show();
                    }
                });

                dialogoRegistrar.show();
                break;

        }
        return true;
    }
}
